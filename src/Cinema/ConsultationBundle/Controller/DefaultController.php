<?php

namespace Cinema\ConsultationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Cinema\ConsultationBundle\Entity\Film;
use Cinema\ConsultationBundle\Entity\Studio;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('CinemaConsultationBundle:Default:index.html.twig', array('name' => $name));
    }

    public function index1Action() {
        $entityManager = $this->getDoctrine()->getManager();
        $film = $entityManager->getRepository('CinemaConsultationBundle:Film')->findAll();
        
        return $this->render('CinemaConsultationBundle:Default:index1.html.twig', array('films' => $film));
    }

    public function listefilmAction() {
        return $this->render('CinemaConsultationBundle:Default:listefilm.html.twig', array());
    }

    public function affichefilmAction($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $film = new Film();
        $film = $entityManager->find('CinemaConsultationBundle:Film', $id);

        echo 'Ce film se nomme ' . $film->getTitre()
        . ' il dure ' . $film->getDuree()
        . 'min et à été fait aux ' . $film->getPays()
        . '. le synopsis est : ' . $film->getResume()
        . '. il a eu ' . $film->getRecette()
        . ' de recette.';

        return $this->render('CinemaConsultationBundle:Default:index.html.twig', array('film' => $film));
    }

    public function affichefilm2Action($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $film = new Film();
        $film = $entityManager->find('CinemaConsultationBundle:Film', $id);

        return $this->render('CinemaConsultationBundle:Default:unfilm.html.twig', array('lefilm' => $film));
    } 

    public function affichelesfilmsAction() {
        $entityManager = $this->getDoctrine()->getManager();
        $film = new Film;
        $film = $entityManager->getRepository('CinemaConsultationBundle:Film')->findAll();

        foreach ($film as $film) {
            echo $film->getTitre() . "<br>";
        }

        return $this->render('CinemaConsultationBundle:Default:index.html.twig', array());
    }

    public function affichelesfilms2Action() {

        $entityManager = $this->getDoctrine()->getManager();
        $film = new Film;
        $film = $entityManager->getRepository('CinemaConsultationBundle:Film')->findAll();

        return $this->render('CinemaConsultationBundle:Default:listefilms.html.twig', array('film' => $film));
    }
/*
    public function ajoutAction() {
        $entityManager = $this->getDoctrine()->getManager();

        $film1 = new Film();
        $film1->setTitre('the Mask');
        $film1->setDuree(101);
        $film1->setRecette(10000000);
        $film1->setPays('usa');
        $film1->setResume('cest drole');
        $entityManager->persist($film1);

        $film2 = new Film();
        $film2->setTitre('django');
        $film2->setDuree(180);
        $film2->setRecette(20000000);
        $film2->setPays('usa');
        $film2->setResume('cest violent');
        $entityManager->persist($film2);

        $stud1 = new Studio();
        $stud1->setNom('enfer');
        $stud1->setPays('allemagne');
        $stud1->setAdresse('666 rue du purgatoire');
        $stud1->setCa(6666666);
        $entityManager->persist($stud1);

        $stud2 = new Studio();
        $stud2->setNom('candyland');
        $stud2->setPays('usa');
        $stud2->setAdresse('quelque pars');
        $stud2->setCa(2000000);
        $entityManager->persist($stud2);

        $entityManager->flush();
        return $this->render('CinemaConsultationBundle:Studio:ajoutstudiofilm.html.twig', array());
    }
*/
    function ajouterAction(Request $request) {
        // création de l'objet Film
                $leFilm = new film();
        // création du FormBuilder avec les champs que l'on souhaite renseigner (sauf l'id)
                $formBuilder = $this->createFormBuilder($leFilm)
                        ->add('titre', TextType::class, array('label' => 'Titre'))
                        ->add('duree', IntegerType::class, array('label' => 'Durée'))
                        ->add('pays', TextType::class, array('label' => 'Pays'))
                        ->add('resume', TextareaType::class, array('label' => 'Résumé','required'=>false))
                        ->add('recette', MoneyType::class, array('label' => 'Recettes','required'=>false))
                        ->add('Insérer', SubmitType::class);
        // génération du formulaire
                $form = $formBuilder->getForm();

                if ($request->isMethod('POST')) {// Vérification : la requête (retour du formulaire) est-elle de type POST - lien Formulaire (vue) <-> Controleur
        // -> les données saisies dans le formulaire sont désormais dans la variable $leFilm
        $form->handleRequest($request);
        // Vérification des valeurs saisies
        if ($form->isValid()) {
        // insertion du film dans en BDD
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($leFilm);
        $entityManager->flush();
        // puis appel au template permettant d'éditer ce nouveau film
        // => redirection vers une autre page (autre "route")
        // définition d'un message flash
        $this->get('session')->getFlashBag()->add('info','Film enregistré');
        return $this->redirectToRoute('cinema_consultation_affichefilm2', ['id' =>
        $leFilm->getId()]);
        }//fin si "valid"
        }// fin si "post"

        // appel à la vue avec en paramètre le formulaire créé
                return $this->render('CinemaConsultationBundle:Default:ajouter.html.twig', array('form' => $form->createView()));
            }

}
