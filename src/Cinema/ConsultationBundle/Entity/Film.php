<?php

namespace Cinema\ConsultationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity @ORM\Table(name="symfony.Film")
 */
class Film {
    
/**
 *@ORM\Id @ORM\Column(name="id",type="integer") @ORM\GeneratedValue
 */
    private $id;
    
    /**
     @ORM\column(name="titre", type="string", length=50)
     */
    private $titre;
    
    /**
     @ORM\column(name="duree", type="integer")
     */
    private $duree;
    
    /**
     @ORM\column(name="pays", type="string", length=15)
     */
    private $pays;
    
    /**
     @ORM\column(name="resume", type="string", length=100)
     */
    private $resume;
    
    /**
    @ORM\column(name="recette", type="integer")
     */
    private $recette;

    function getId() {
        return $this->id;
    }

    function getTitre() {
        return $this->titre;
    }

    function getDuree() {
        return $this->duree;
    }

    function getPays() {
        return $this->pays;
    }

    function getResume() {
        return $this->resume;
    }
    function getRecette() {
        return $this->recette;
    }

        function setId(int $id): void {
        $this->id = $id;
    }

    function setTitre(string $titre): void {
        $this->titre = $titre;
    }

    function setDuree(int $duree): void {
        $this->duree = $duree;
    }

    function setPays(string $pays): void {
        $this->pays = $pays;
    }

    function setResume(string $resume): void {
        $this->resume = $resume;
    }
    function setRecette($recette): void {
        $this->recette = $recette;
    }

    

}
?>
