<?php

namespace Cinema\ConsultationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity @ORM\Table(name="symfony.Studio")
 */
class Studio {

    /**
     * @ORM\Id @ORM\Column(name="id",type="integer") @ORM\GeneratedValue
     */
    private $id;
    
    /**
     @ORM\column(name="nom", type="string", length=20)
     */
    private $nom;
    
    /**
     @ORM\column(name="pays", type="string", length=15)
     */
    private $pays;
    
    /**
     @ORM\column(name="adresse", type="string", length=50)
     */
    private $adresse;
    
    /**
    @ORM\column(name="ca", type="integer")
     */
    private $ca;
    
    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPays() {
        return $this->pays;
    }

    function getAdresse() {
        return $this->adresse;
    }
    
    function getCa() {
        return $this->ca;
    }

        function setId($id): void {
        $this->id = $id;
    }

    function setNom($nom): void {
        $this->nom = $nom;
    }

    function setPays($pays): void {
        $this->pays = $pays;
    }

    function setAdresse($adresse): void {
        $this->adresse = $adresse;
    }
    function setCa($ca): void {
        $this->ca = $ca;
    }



}

?>